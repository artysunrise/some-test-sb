var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.dev.config');
var express = require("express");
var path = require("path");

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true,
  contentBase: "dist/",
  proxy: {
    "/api*": {
      target: "http://localhost:3000",
    },
  }
}).listen(5000, 'localhost', function (err, result) {
  if (err) {
    return console.log(err);
  }

  console.log('Frontend Listening at http://localhost:5000/. Open this link');
});

// SERVER - мок бэкэнда (для раздачи JSON-ов)
var app = express();
app.use("/", express.static(path.join(__dirname, "dist")));

app.get('/api/directories', function (req, res) {
  var directories = require('./json/getDirectories');
  res.json(directories);
});

app.get('/api/directories/:id', (req, res) => {
  const directory = require('./json/getDirectory');
  res.json(directory);
});

app.get('/api/directories/:id/entries/:entryId', (req, res) => {
  const entry = require('./json/getEntry');
  res.json(entry);
});

app.listen(3000, function () {
  console.log('Backend mock server listening on port 3000');
});
