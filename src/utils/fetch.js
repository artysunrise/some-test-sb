import "whatwg-fetch";

// TODO: так неправильно делать, нужно сделать расширение, а не замену
const defaultOptions = {
  method: "GET",
}

export default async function Fetch(url, options = defaultOptions) {
  const response = await window.fetch(url, options);

  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return await response.json();
}
