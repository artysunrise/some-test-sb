import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import DirectoriesReducer from '../modules/directories/reducer';
import DirectoryReducer from '../modules/directory/reducer';
import EntryReducer from '../modules/entry/reducer';

const createStoreWithMiddleware = applyMiddleware(thunkMiddleware)(createStore);
const reducers = {
  Directories: DirectoriesReducer,
  Directory: DirectoryReducer,
  Entry: EntryReducer,
}

const Reducers = combineReducers(reducers);

export function configureStore(initialState) {
  return createStoreWithMiddleware(Reducers, initialState);
}
