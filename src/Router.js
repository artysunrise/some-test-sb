import React from "react";
import { Router, Route, hashHistory } from "react-router";

import Directories from "./modules/directories/container";
import Directory from "./modules/directory/container";
import Entry from "./modules/entry/container";

export default function AppRouter() {
  return (
    <Router history={hashHistory}>
      <Route path="/" component={Directories} />
      <Route path="/directories/:directoryId" component={Directory} />
      <Route path="/directories/:directoryId/entries/:entryId" component={Entry} />
    </Router>
  )
}
