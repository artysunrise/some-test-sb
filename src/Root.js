import React from "react";
import {Provider} from "react-redux";
import {configureStore} from "./store";
import Router from "./Router";

export default function Root() {
  return (
    <Provider store={configureStore()}>
      <Router />
    </Provider>
  );
}
