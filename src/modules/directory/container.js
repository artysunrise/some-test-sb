import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as ActionCreators from "./actions";
import Directory from "./component";

class DirectoryContainer extends React.Component {
  componentDidMount () {
    const {directoryId} = this.props.params;
    this.props.actions.fetchDirectory(directoryId);
  }

  render() {
    return (
      <Directory {...this.props} />
    )
  }
}

export default connect(
  (state) => state.Directory,
  (dispatch) => ({actions: bindActionCreators(ActionCreators, dispatch)}),
)(DirectoryContainer);
