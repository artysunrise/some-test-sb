import React from "react";
import { Link } from "react-router";

import commonStyles from 'common/styles.less';
import styles from './directory.less';

function DirectoryTable({id: directoryId, fields, items}) {
  return (
    <table className={styles.directoryTable}>
      <thead>
        <tr>
          {
            fields.map(item => (
              <th key={item.id}>{item.name}</th>
            ))
          }
        </tr>
      </thead>
      <tbody>
        {
          items.length === 0 &&
            <tr><td colSpan={fields.length}>Нет данных</td></tr>
        }

        {
          items.map(item => (
            <tr key={item.id}>
              {
                fields.map(field => (
                  <td key={field.id}>
                    {
                      field.id === "name"
                      ? <Link to={`/directories/${directoryId}/entries/${item.id}`}>{item[field.id]}</Link>
                      : item[field.id]
                    }
                  </td>
                ))
              }
            </tr>
          ))
        }
      </tbody>
    </table>
  )
}


export default function Directory(props) {
  const {directory, filterText} = props;

  // TODO: loading-spinner
  if (!directory.id) {
    return <span />;
  }

  return (
    <div>
      <div>
        <Link to="/">Справочники</Link>
        <span>→</span>
      </div>
      <h1>{directory.name}</h1>

      <div className={styles.row}>
        <button className={`${commonStyles.btnSuccess}`}>+ Создать новый</button>
      </div>

      <div className={commonStyles.flContainer}>
        <DirectoryTable {...directory} />

        <div className={styles.filterBlock}>
          <span>Фильтр</span>
          <form>
            <input type="text" placeholder="Поиск" value={filterText}
              onChange={e => props.actions.filterChanged(e.target.value)} />
          </form>
        </div>
      </div>
    </div>
  )
}
