import Fetch from "utils/fetch";
import * as constants from "./constants";

const BASE_URL = "/api/directories";

export function fetchDirectory(directoryId) {
  return async (dispatch) => {
    try {
      const url = `${BASE_URL}/${directoryId}`;
      const {response: {directory}} = await Fetch(url);
      dispatch({type: constants.DIRECTORY_FETCHED, directory});
    } catch (err) {
      console.error(err.stack);
    }
  };
}

export function filterChanged(filterText) {
  return {
    type: constants.DIRECTORY_FILTER_CHANGED,
    filterText,
  }
}
