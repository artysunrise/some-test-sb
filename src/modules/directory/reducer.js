import * as Actions from "./constants";

const initialState = {
  directory: {},
  immutableDirectory: {},
  filterText: "",
};

function DirectoryReducer(state = initialState, action) {
  switch (action.type) {

    case Actions.DIRECTORY_FETCHED:
      return {
        ...state,
        directory: action.directory,
        immutableDirectory: action.directory,
      }

    case Actions.DIRECTORY_FILTER_CHANGED:
      const filterText = action.filterText;
      let items;

      if (filterText.length) {
        items = state.immutableDirectory.items.filter(item =>
          item.name.toLowerCase().includes(filterText.toLowerCase()),
        );
      } else {
        items = [...state.immutableDirectory.items];
      }
      return {
        ...state,
        filterText: action.filterText,
        directory: {
          ...state.directory,
          items,
        },
      }

    default:
      return state;
  }
}

export default DirectoryReducer;
