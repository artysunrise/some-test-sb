import * as Actions from "./constants";

const initialState = {
  entry: {},
};

function DirectoryReducer(state = initialState, action) {
  switch (action.type) {

    case Actions.ENTRY_FETCHED:
      return {
        ...state,
        entry: action.entry,
      }

    default:
      return state;
  }
}

export default DirectoryReducer;
