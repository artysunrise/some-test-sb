import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as ActionCreators from "./actions";
import Entry from "./components";

class EntryContainer extends React.Component {
  componentDidMount () {
    const {directoryId, entryId} = this.props.params;
    this.props.actions.fetchEntry(directoryId, entryId);
  }

  render() {
    return (
      <Entry {...this.props} />
    )
  }
}

export default connect(
  (state) => state.Entry,
  (dispatch) => ({actions: bindActionCreators(ActionCreators, dispatch)}),
)(EntryContainer);
