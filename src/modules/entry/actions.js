import Fetch from "utils/fetch";
import * as constants from "./constants";

const BASE_URL = "/api/directories";

export function fetchEntry(directoryId, entryId) {
  return async (dispatch) => {
    try {
      const url = `${BASE_URL}/${directoryId}/entries/${entryId}`;
      const {response: {entry}} = await Fetch(url);
      dispatch({type: constants.ENTRY_FETCHED, entry});
    } catch (err) {
      console.error(err.stack);
    }
  };
}
