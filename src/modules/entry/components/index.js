import React from "react";
import { Link } from "react-router";
import Form from "./form";

export default function Entry(props) {
  const {entry} = props;

  // TODO: loading-spinner
  if (!entry.id) {
    return <span />;
  }

  return (
    <div>
      <div>
        <Link to="/">Справочники</Link>
        <span>→</span>
        <Link to={`/directories/${entry.directory.id}`}>{entry.directory.name}</Link>
        <span>→</span>
      </div>

      <h1>{entry.name}</h1>

      <Form {...entry} />

    </div>
  )
}
