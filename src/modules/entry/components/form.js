import React from "react";
import { Link } from "react-router";

import commonStyles from 'common/styles.less';
import styles from '../entry.less';

function FormRow({id, name, type, value, values}) {
  if (!["text", "integer", "float", "date", "textarea", "select"].includes(type.toLowerCase())) {
    throw new Error(`Unknown control type given: ${type}`);
  }

  return (
    <div className={`${commonStyles.flContainer} ${styles.formRow}`}>
      <label htmlFor={id}>{name}</label>
      <div>
        {
          (["integer", "float"].includes(type.toLowerCase())) &&
            <input id={id} type="number" defaultValue={value} />
        }

        {
          (type.toLowerCase() === "text") &&
            <input id={id} type="text" defaultValue={value} />
        }

        {
          (type.toLowerCase() === "textarea") &&
            <textarea id={id} defaultValue={value}/>
        }

        {
          // TODO: CALENDAR
          (type.toLowerCase() === "date") &&
            <div>
              <input id={id} type="text" defaultValue={value} />
            </div>
        }

        {
          (type.toLowerCase() === "select") &&
            <select id={id} defaultValue={values[0].id}>
              {
                values.map(item => (
                  <option value={item.id} key={item.id}>{item.name}</option>
                ))
              }
            </select>
        }
      </div>
    </div>
  )
}

export default function Form({items, directory}) {
  return (
    <form>
      {
        items.map(item => (
          <FormRow key={item.id} {...item} />
        ))
      }

      <div className={styles.buttonsContainer}>
        <Link to={`/directories/${directory.id}`} className={`${commonStyles.btnSuccess}`} type="button">Сохранить</Link>
        <Link to={`/directories/${directory.id}`} className={commonStyles.btn} type="button">Отменить изменения</Link>
        <Link to={`/directories/${directory.id}`} className={commonStyles.btn} type="button">Удалить</Link>
      </div>
    </form>
  )
}