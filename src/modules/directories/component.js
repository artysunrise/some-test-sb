import React from "react";
import { Link } from "react-router";
import styles from "./directories.less";

export default function Directories(props) {
  const {directories} = props;

  return (
    <div>
      <h1>Cправочники</h1>

      <ul className={styles.directorisList}>
        {
          directories.map(item => (
            <li key={item.id}>
              <Link to={`/directories/${item.id}`}>{item.name}</Link>
            </li>
          ))
        }
      </ul>
    </div>
  )
}
