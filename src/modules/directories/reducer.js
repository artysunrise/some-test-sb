import * as Actions from "./constants";

const initialState = {
  directories: [],
};


function DirectoriesReducer(state = initialState, action) {
  switch (action.type) {

    case Actions.DIRECTORIES_FETCHED:
      return {
        ...state,
        directories: action.directories,
      }

/*    case Actions.SELECT_ACCOUNT:
      return {
        ...state,
        currentAccount: action.account,
        currentAccountData: null,
      };
*/
    default:
      return state;
  }
}

export default DirectoriesReducer;
