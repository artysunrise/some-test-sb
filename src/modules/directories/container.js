import React from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as ActionCreators from "./actions";
import Directories from "./component";

class DirectoriesContainer extends React.Component {
  componentDidMount () {
    this.props.actions.fetchDirectores();
  }

  render() {
    return (
      <Directories {...this.props} />
    )
  }
}

export default connect(
  (state) => state.Directories,
  (dispatch) => ({actions: bindActionCreators(ActionCreators, dispatch)}),
)(DirectoriesContainer);
