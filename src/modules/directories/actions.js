import Fetch from "utils/fetch";
import * as constants from "./constants";

const BASE_URL = "/api/directories";

export function fetchDirectores() {
  return async (dispatch) => {
    try {
      const url = `${BASE_URL}`;
      const {response: {directories}} = await Fetch(url);
      dispatch({type: constants.DIRECTORIES_FETCHED, directories});
    } catch (err) {
      console.error(err.stack);
    }
  };
}
