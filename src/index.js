import ReactDom from "react-dom";
import React from "react";
import Root from "./Root";

function createRoot() {
  const rootDiv = document.createElement("div");
  rootDiv.id = "root";
  document.body.appendChild(rootDiv);
  return rootDiv;
}

ReactDom.render(<Root />, createRoot());
